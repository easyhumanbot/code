'use strict';

angular.module('app')
  .config(function($stateProvider) {
    $stateProvider
      .state('project_delete', {
        url: '/project/:id/delete',
        controller: 'ProjectDeleteController as projectDeleteCtrl',
        parent: 'default',
      })
      .state('project_list', {
        url: '/project',
        templateUrl: 'project/project_list.html',
        controller: 'ProjectListController as projectListCtrl',
        parent: 'default',
      })
      .state('project_add', {
        url: '/project/add',
        templateUrl: 'project/project_add.html',
        controller: 'ProjectAddController as projectAddCtrl',
        parent: 'default',
      })
      .state('project_edit', {
        url: '/project/:id/edit',
        templateUrl: 'project/project_add.html',
        controller: 'ProjectEditController as projectEditCtrl',
        parent: 'default',
      });
  });
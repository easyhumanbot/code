'use strict';
angular.module('app')
  /**
  * @controller Project
  * @method ProjectListController
  * @description
  * List
  */
  .controller('ProjectListController', function($scope, $rootScope, $state, $timeout, $q, $stateParams, Flash, Project) {
    $scope.window_id = 'project_list'
    $scope.model = Project.createForm();

    var modalAdd;
    $scope.add = function() {
      if (modalAdd) modalAdd.closeForm();
      var template = '<div ng-if="show" class="col-xs-12 col-md-6" style="position:absolute;right:0;top:15px" ng-controller="ProjectAddController" ng-include="\'project/project_add.html\'"></div>';
      modalAdd = $rootScope.createModalWindow(template)
      modalAdd.closeForm = function(updateList) {
        if (updateList) update()
        modalAdd.show = false;
      }
      modalAdd.show = true
    }

    var modalEdit;
    $scope.edit = function(id) {
      if (modalEdit) modalEdit.closeForm();
      modalEdit = $rootScope.createModalWindow('<div ng-if="edit_id" class="col-xs-12 col-md-6" style="position:absolute;right:0;top:15px" ng-controller="ProjectEditController" ng-include="\'project/project_add.html\'"></div>');
      modalEdit.closeForm = function(updateList) {
        if (updateList) update()
        modalEdit.edit_id = null;
      }
      modalEdit.edit_id = id;
    }

    var update = function() {
      if ($scope.isSearching) return;
      var search = $scope.search;
      $scope.model.service.page = (pagination != null) ? pagination.scope.getPage() : 1
      $scope.isSearching = true;
      if (!search) search = ''
      $scope.records = []

      var query = Project._query()
        .or('name', search)
      query.execute(function(data, ids) {
          $scope.records = data;
          $scope.isSearching = false;
      })
      query.count(function(count) {
          pagination.scope.setPageCount( Math.round(count / Project.pageSize) )
      });
    }

    var pagination;
    angular.element(document).ready(function () {
        pagination = $rootScope.createComponent({
            selector: 'pagination-component-project',
            component: 'PaginationComponent',
            template: 'components/pagination.html',
        })
        pagination.scope.onSetPageEvent = function(page) {
            update()
        };
        update();
    });

    var lastKeyPress = 0;
    $scope.searchTable = function() {
      lastKeyPress = (new Date()).getTime()
      setTimeout(function() {
        if (((new Date()).getTime() - lastKeyPress) > 700) {
          $scope.current_page = 1;
          update()
        }
      }, 750)
    }

    $scope.delete = function(item_id) {
      var entry = Project.get({id: item_id}, function(data) {
        entry.$delete({id: item_id}, function() {
          var index = $scope.records.indexOfByKey('_id', item_id);
          //$scope.records.splice(index, 1);
          $scope.records[index]._deleted = true;
          $timeout(function() {
            $scope.$apply()
          });
          Flash.notify("Удалено")
        })
      });
    }

    $scope.restore = function(item_id) {
      var index = $scope.records.indexOfByKey('_id', item_id);
      var data = $scope.records[index]
      Project.save(data, function(res) {
        $scope.records[index]._deleted = false;
        $timeout(function() {
          $scope.$apply()
        });
        Flash.notify("Восстановлено")
      });
    }

  })


  /**
  * @controller Project
  * @method ProjectFormController
  * @description
  * Form controller executed in add, edit form pages
  */
  .controller('ProjectFormController', function($compile, $rootScope, $scope, $timeout, UserAuth) {
    $scope.user = UserAuth.user;

    //Select client
    var template = '<div ng-if="showModal" ng-init="window_id=\'contact_list_modal\'" ng-controller="ContactListController" ng-include="\'contact/contact_list.html\'"></div>';
    var clientModal = $rootScope.createModalWindow(template)
    clientModal.selectRecord = function(record) {
      $scope.form.fields._clientId.ajaxLoad();
      $scope.form.fields._clientId.value = record._id
      clientModal.closeModal();
    }
    clientModal.toggle = function() {
      clientModal.showModal = !clientModal.showModal
    }
    $scope.toggleClientModal = clientModal.closeModal = clientModal.toggle

    //Select tasks
    var template = '<div ng-if="showModal" ng-init="window_id=\'task_list_modal\'" ng-controller="TaskListController" ng-include="\'task/task_list.html\'"></div>';
    var taskModal = $rootScope.createModalWindow(template)
    taskModal.selectRecord = function(record) {
      $scope.form.fields._tasks.ajaxLoad();
      $scope.form.fields._tasks.toggleValue(record._id)
    }
    taskModal.toggle = function() {
      taskModal.showModal = !taskModal.showModal
    }
    $scope.toggleTaskModal = taskModal.closeModal = taskModal.toggle

    //Edit client
    var modalEdit;
    $scope.editClient = function(id) {
      if (modalEdit) modalEdit.closeForm();
      modalEdit = $rootScope.createModalWindow('<div ng-if="edit_id" ng-init="window_id=\'contact_edit_modal\'" class="col-xs-12 col-md-6 window-modal" ng-controller="ContactEditController" ng-include="\'contact/contact_add.html\'"></div>');
      modalEdit.closeForm = function(updateList) {
        $scope.form.fields._clientId.ajaxLoad();
        modalEdit.edit_id = null;
      }
      modalEdit.edit_id = id;
    }
  })

  /**
  * @controller Project
  * @method ProjectAddController
  * @description
  * Add record
  */
  .controller('ProjectAddController', function($scope, $state, $stateParams, Flash, Project) {
    $scope.form = Project.createForm();
    $scope.window_id = 'project_add'

    $scope.form.submit = function() {
      var ngform = $scope.form.getNgForm($scope);
      ngform.$submitted = true; // don't work like form
      if (ngform.$invalid) {
        return;
      }
      $scope.form.getFieldData(function(data) {
        delete data["_id"];

        Project.save(data, function(res) {
          Flash.notify("Success")
          $scope.closeForm(true)
        });
      });

    }
  })

  /**
  * @controller Project
  * @method ProjectEditController
  * @description
  * Edit record
  */
  .controller('ProjectEditController', function($scope, $state, $timeout, $stateParams, Flash, Project) {
    $scope.form = Project.createForm();
    $scope.window_id = 'project_add'

    var _id = $scope.edit_id ? $scope.edit_id : $stateParams.id;

    var entry = Project.get({id: _id}, function(data) {
      $scope.form.setFieldData(data);
      $timeout(function() {
        $scope.$apply();
      });
    }, function(error) {
      Flash.error(error.statusText)
    });

    $scope.form.submit = function() {
      var ngform = $scope.form.getNgForm($scope);
      ngform.$submitted = true; // don't work like form
      if (ngform.$invalid) {
        return;
      }

      $scope.form.getFieldData(function(data) {
        angular.forEach(data, function(value, key) {
          entry[key] = data[key]
        })
        entry.$update({id: data._id}, function() {
          Flash.notify("Success")
          $scope.closeForm(true)
        })
      });
    }
  })
'use strict';

angular.module('app')
  .service('Project', function(ProjectU, ResourceWrapper) {
    return ResourceWrapper(ProjectU)
  })

  .service('ProjectU', function ($resource, serviceRootUrl, crud) {
    var headerPage = function() {
      return service.page ? service.page : 1
    }

    var headerPageSize = function() {
      return service.pageSize;
    }

    var service = $resource(serviceRootUrl + '/projects/:id', {}, {
      query: { method: 'GET', isArray: true, headers: {page: headerPage, 'page-size': headerPageSize} },
      count: { method: 'GET' },
      update: { method: 'PUT' }
    });
    service.pageSize = 10;

    service.createForm = function () {
      var form = angular.copy(crud.getModel('project'));
      return form;
    }
    return service;
  });

/*
'use strict';

angular.module('app')
  .service('Project', function(ProjectU, ResourceWrapper) {
    return ResourceWrapper(ProjectU)
  })

  .service('ProjectU', function($resource, serviceRootUrl, crud, $localStorage) {
    function Service() { }
    var service = new Service();

    service.pageSize = 10;

    if (!$localStorage.data) $localStorage.data = {}
    if (!$localStorage.data.project) $localStorage.data.project = []

    service.query = function(q, callback, errorCallback) {
      if (typeof q == 'function') {
        errorCallback = callback
        callback = q
      }
      var search = "";
      if (q.find) {
        search = q.find.$or[0].name.$regex
      }
      var result = $localStorage.data.project.filter(function(item) {
        if (!item.name) {
          return true
        }

        if (item.name.search(new RegExp(search, "i")) == -1) {
          return false
        }
        return true
      })
      callback(result);
    }

    service.count = function(q, callback, errorCallback) {
      callback({count: $localStorage.data.project.length})
    }


    service.save = function(object, callback, errorCallback) {
      object._id = ($localStorage.data.project.length + 1).toString();
      $localStorage.data.project.push(object)
      callback()
    }

    service.get = function(q, callback, errorCallback) {
      var index = $localStorage.data.project.indexOfByKey("_id", q.id)
      var object = $localStorage.data.project[index]

      object.$update = function(data, callback) {
        console.log("no update")
        callback();
      }

      object.$delete = function(object, callback) {
        $localStorage.data.project.splice(index, 1)
        callback()
      }
      setTimeout(function() {
        callback(object)
      }, 100)

      return object;
    }

    service.createForm = function() {
      var form = angular.copy(crud.getModel('project'));
      return form;
    }


    return service;
  })

*/